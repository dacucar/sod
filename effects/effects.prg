Import "mod_video"
Import "mod_screen"
Import "mod_proc"
Import "mod_map"
Import "mod_key"
Import "mod_rand"

Global
	g; stop;
End

Begin
	full_screen = false;
	set_mode(640, 480, 16);
	g = load_map("bg.map");
	set_center(0,g, 0, 0);
	
	Repeat
		graph = g;
		
		If(scan_code == _1) efecto(0); End
		If(scan_code == _2) efecto(1); End
		If(scan_code == _3) efecto(2); End
		If(scan_code == _0) stop = true; End
		
		While(scan_code <> 0) Frame; End
		Frame; stop = false;
	Until(key(_ESC))
	let_me_alone();
End


Process efecto(f)
Private
	a,b,c,d;
	pointer q;
	pointer w;
End
Begin
	Switch(f)
		Case 0:
			fade(200, 30, 30, 7);
			While(fading) Frame; End
			fade(100, 100, 100, 4);
			While(fading) Frame; End
		End
		Case 1:
			z=-255;
    		set_center(0,graph, 640 >> 1, 480 >> 1);
    		flags = 4;
    		Repeat
    			graph = get_screen();	
       			x=(640 >> 1)+rand(-20, 20);
        		y=(480 >> 1)+rand(-5, 5);
        		Frame;
        		unload_map(0,graph);
    		Until(stop == true)
    		stop = false;
    	End
	End
End
