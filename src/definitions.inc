/*------------------------------------------------------------------------------
 Shadow Of Dragon: RPG OpenSource programado en Fenix
 Copyright(C) 2003 SoD Team
 Fecha Inicio: 24/06/2002 (15:27:56h)
 Contacto: lord_danko@users.sourceforge.net
--------------------------------------------------------------------------------

 Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los 
 t�rminos de la Licencia P�blica General de GNU seg�n es publicada por la Free 
 Software Foundation, bien de la versi�n 2 de dicha Licencia o bien (seg�n su 
 elecci�n) de cualquier versi�n posterior. 

 Este programa se distribuye con la esperanza de que sea �til, pero SIN NINGUNA 
 GARANT�A, incluso sin la garant�a MERCANTIL impl�cita o sin garantizar la 
 CONVENIENCIA PARA UN PROP�SITO PARTICULAR. V�ase la Licencia P�blica General 
 de GNU para m�s detalles. 

 Deber�a haber recibido una copia de la Licencia P�blica General junto con este 
 programa. Si no ha sido as�, escriba a la Free Software Foundation, Inc., en 675 
 Mass Ave, Cambridge, MA 02139, EEUU.

--------------------------------------------------------------------------------
 Archivo: definitions.inc
 Descripci�n: Variables, constantes y tipos
 Ultima modificaci�n: 24-08-2003
------------------------------------------------------------------------------*/

import "mod_key"
import "mod_map"
import "mod_video"
import "mod_screen"
import "mod_sound"
import "mod_proc"
import "mod_mem"
import "mod_math"
import "mod_text"
import "mod_say"
import "mod_timers"
import "mod_string"
import "mod_joy"
import "mod_file"
import "mod_rand"
import "mod_mouse"
import "mod_dir"
import "mod_scroll"

type color
     int r,g,b;
end

type tEnemigo
	string nombre = "Malo";
	int gr�fico;
	int pv; pv_totales;
    int fuerza;
    int resistencia;
    int velocidad;
    byte estado;
    x,y;		//id del proceso que le cre�
end

//Arrays Din�micos
//------------------------------------------------------------------------------
type i_array
	pointer el;
	int len;
	int st=0;
end

type b_array
	pointer el;
	int len;
	int st=0;
end

type w_array
	pointer el;
	int len;
	int st=0;
end
//------------------------------------------------------------------------------

// DEFINICIONES DEL Writer Deluxe

//-------------------INCLUIR EN LA SECCI�N DECLARACIONES DE TIPOS---------------------
type __rWDClass
	int rFuente=0;			//fuente que se usar�
    string rTexto="";	//texto a escribir
    string rIntroChar="&";	//caracter de retorno (para simular la tecla especificada
    					// por rIntroKey ya que los textos cargados se cojen de 
    					// una sola l�nea
    int rIntroKey=0;		//C�digo ascii de la tecla que se usa para continuar escribiendo
    					//usar 0 para cualquier tecla.
	
	int rMaxCaract=20; 		//caracteres por l�nea
	int rMaxRenglones=3;	//m�ximo de renglones por parrafo
	
	int rEspaciado=0;		//distancia entre caracteres (adicional al ancho de cada letra)
	int rEspacios=2;		//distancia adicional de los espacios (huecos entre palabras)
	int rDistY=0;			//dist�ncia entre l�neas (adicional al tama�o de una letra 
						//may�scula
	
	int rVel=15;			//velocidad general
	int rVelPto=100;		//velocidad de pausa despu�s de un punto
	int rVelComa=40;		//velocidad de pausa despu�s de coma 
	int rVelPtoComa=22;		//velocidad de pausa despu�s de punto y coma
	
	int rPosx=0;			//Coordenada X de donde se escribir� (o se pondr� la imagen)
	int rPosy=0;			//Coordenada Y de donde se escribir� (o se pondr� la imagen)
	
	int rMargenI=0;			//margen izquierdo desde rPosX hasta donde comienza el texto
	int rMargenS=0;			//margen superior desde rPosy hasta donde comienza el texto
	
	int rTotGraphics=0;		//n�mero de gr�ficos que se usar�n 0 para ninguno. Si se quiere
						//un gr�fico para cuando hay una l�nea y otro para lo dem�s,
						//se deber�a poner un 2 (ejemplo). SI se quiere 1 solo gr�fico
						//para todo, debe valer 1.
	int rGraphics[0];	    //array para cargar los distintos gr�ficos. Van del siguiente
						//modo: gr�fico para una l�nea rGraphics[|], gr�fico para 2
						//l�neas rGraphics[2], etc. El gr�fico que se muestra cuando
						//no hay disponible para X l�neas es el 0.
						
	byte rUseEffects=0;	//0=No usar efectos de entrada. >0 Usar efectos de entrada
	int rEffects[1]=0,0;	//array de efectos. 1 elemento: 0=no trasparente,>0 trasparente
						//2 elemento: 0 no usar efecto de entrada, >0 n�mero del 
						//efecto de entrada a usar.	
	
	rExitEffect=0;		//0=no usar efectos de salida. >0 n�mero del efecto a usar
end	
//----------------FIN INCLUIR EN LA SECCI�N DECLARACIONES DE TIPOS---------------------

const
  //----------------INCLUIR EN LA SECCI�N DECLARACIONES DE COSTANTES---------------------	
	__rTT=100;			//pon aqu� un valor que sea mayor al n�mero de letras (contando
						//espacios) que tendr�s en pantalla al mismo tiempo. Un n�mero
						//mu alto consumir�a m�s mem�ria que uno m�s bajo
	
	__rTL=100;			//Si sabes el n�mero m�ximo de renglones que ocupar�n tus textos
						//ponlo aqu� (ahorrar�s mem�ria)						
  //--------------FIN INCLUIR EN LA SECCI�N DECLARACIONES DE COSTANTES-------------------

  //constantes del juego
    c_n_armas = 6;       //n�mero m�ximo de armas por personaje
    c_n_brazaletes = 15; //n�mero m�ximo de brazaletes por personaje
    c_n_magias = 30;
    c_n_invoc = 10; 
    c_n_objetos = 10;	 //n�mero m�ximo de objetos por lista                        
  
  //constantes para los estados del personaje
    _CNORMAL = 0;
    _CENVENENADO = 1;
    _CMUERTO = 2;
    _CDORMIDO = 3;

  //constantes para los tipos de objetos
    _OATAQUE = 0;
    _OCURA = 1;
    _OMAGICOS = 2;
    _OMEJORA = 3;
    _OCLAVE = 4;
    _OOTROS = 5;
  
  //constantes para las vatallas
  	_VBASE=1000;	//velocidad base en milisec

  //otras constantes
  	fon=1;	//para los fades
  	foff=0;	//para los fades
  	Si=0;
  	No=1;
  	
  	_z1=2;_z2=3; //costantes para la z
  	
  	

global
//fpgs
    fpg_menu_principal;fpg_menu;fpg_caras;fpg_titulo;fpg_personajes;fpg_ranuras;
    fpg_fase;fpg_durezas;fpg_intros;fpg_batallas;
    fnt_textos;fnt_numeros; 
    fnt_act1; //fuente 1 actualmente seleccionada
    fnt_act2; //fuente 2 actualmente seleccionada

//sonidos
    sonidos[2];
	
//variables del control de las teclas, para que se pueda redefinir
    t_arriba=72;
    t_abajo=80;
    t_derecha=77;
    t_izquierda=75;
    t_aceptar=28;
    t_cancelar=83;
    t_menu=_m;

//variables para el control de las colisiones (mapas de durezas)
    color color_bloq=159,0,0;
    colord[2];
    color color_puerta=56,60,0;

//Variables para el control de diversos asepectos del prota: velocidad,altura...
    byte pg_propiedades[2]=4,12,24;
                                
//Variables para el control de los gr�ficos del prota
    byte pg_arriba[2]=4,5,6;
    byte pg_abajo[2]=1,2,3;
    byte pg_derecha[2]=7,8,9;
    byte pg_izquierda[2]=10,11,12;

//           ESTRUCTURA DE LOS PERSONAJES

    struct Personaje[5]
        lo_tengo;         //si lo tenemos
        cara_x;cara_y;    //posicion de las caras en el menu
        string  nombre = "A�n no tienes �ste personaje";
        pv; pv_totales;
        pm; pm_totales;
        fuerza;
        resistencia;
        velocidad;
        byte estado;
        byte ident_arma[1];//el arma que lleva el personaje
    end
//Fin estructura personajes

// ESTRUCTURA DE LAS ARMAS
    struct armas[c_n_armas*6] //30 armas + la nula
        string nombre;      
        fuerza;             
        defensa;            
        suerte;             
        rapidez;            
        peso;               
        gr�fico;            
        byte num_personaje; //identificador del personaje que puede llevar el arma
        byte ranuras[1];    //Ranuras totales del arma y ranuras ocupadas
        la_tengo=0;         //para saber si hemos conseguido el arma
        byte ranura[6];     //guarda los ids de las magias enlazadas
    end
//Fin estructura armas

/*ESTRUCTURA DE LOS BRAZALETES
Los brazaletes son objetos defensivos. Cada personaje puede llevar un arma y un brazalete. Los
brazaletes aportan defensa, suerte, estados, y cosas desas y disponen tambi�n de ranuras al igual
que las armas. Se diferencian principalmente en que son comunes, es decir, todos los personajes
los pueden llevar.
*/
    struct brazaletes[15]   //15 brazaletes + el nulo(con los datos a 0)
        string nombre;      
        byte ranuras[1];    //identico a lo de las armas
        gr�fico;            
        aporto[6];          /*las propiedades q aporta el brazalete
                            0=p_Vida, 1=P_magia, 2=fuerza,
                            3= resistencia, 4=suerte,5=velocidad,6=estado */
        byte num_personaje; //para saber que personaje lo tiene, 0 = ninguno
        lo_tengo=0;         
        byte ranura[6];     //guarda los ids de las invocaciones enlazadas
    end
//Fin estructura brazaletes

//ESTRUCTURA DE LAS MAGIAS
    struct magias[c_n_magias]
        string nombre;
        aporto[6];             /*las propiedades q aporta la magia
                                 0=p_Vida, 1=P_magia, 2=fuerza, 3= resistencia,
                                 4=suerte,5=velocidad,6=estado.valores<0 para quitar ptos*/
        la_tengo=0;
        num_personaje;         //para saber que personaje lo tiene, 0 = ninguno
        nivel;
        word tipo;
        int gasto_pm;
        fuera_batalla;

    end
//Fin estructuras magias

//ESTRUCTURA DE LAS INVOCACIONES
    struct invoc[c_n_invoc]
        string nombre;
        aporto[6];             /*las propiedades q aporta la invoc
                                 0=p_Vida, 1=P_magia, 2=fuerza, 3= resistencia,
                                 4=suerte,5=velocidad,6=estado.valores<0 para quitar ptos*/
        la_tengo=0;
        num_personaje;         //para saber que personaje la tiene, 0 = ninguno
        nivel;
        gasto_pm;
        string tipo;

    end
//Fin

//ESTRUCTURA DE LOS OBJETOS
    struct objetos[c_n_objetos*6]
        string nombre;
        aporto[6];
        lo_tengo=0;
        byte tipo;
    end
//FIN

//    VARIABLES GENERALES
	i_array id_textos;	//Array din�mico para poder borrar los textos que escribamos
	i_array id_procesos; //Guardar� los ids de procesos que queden hu�rfanos
//    VARIABLES DE CONTROL DEL JUEGO
//-----------------------------------------------------------------------------

//regiones

//Gr�ficos
	//g_negro; //para los fades
    g_fase; //gr�fico de la fase
    g_durezas; //grafico de la dureza de la gase
    id_grafico_fondo_anim_princip;
	g_mensajes; //gr�fico para los mensajes
	
	
//Canciones
    id_canci�n_anim_princip;
    id_cancion=-1; 				//id de la canci�n actual

//Pantallas
//He nombrado pantalla al conjunto de varios mapas que juntos forman una casa
//, un pueblo, etc.
    struct pantallas[10]
        px=0;
        py=0;
        string nombre;
    end
    
    p_actual;  //la pantalla en la que estamos
           

//    VARIABLES DEL CONTROL DE BATALLAS
//-----------------------------------------------------------------------------

	int g_fbatalla; 			//gr�fico para el fondo de las batallas
	int g_mbatalla;				//c�pia del menu de la batalla del fpg
	
	int protas_b[3]=1,0,1,2;	//protas q participaran en batlla, el primer campo es
							// para indicar el n�mero total
	
	Byte menu_b=false;		//para saber cuando se ha de poner el cursor en el men�							
	
	Byte orden_batalla[5]; 	//a quien le toca atacar
	int orden_protas[2]=-1,-1,-1;
	byte num_luchadores;   	//n�mero de luchadores q hay en la batalla
	int haya_atacado[5]; 		//si un num_prota ha terminado de atacar
	
	byte num_enemigos;		//n�mero de enemigos
	tEnemigo Enemigos[2];	//enemigos
	

//    VARIABLES DEL CONTROL DEL MNU
//-----------------------------------------------------------------------------

//regiones
    reg_lst_p_armas[1];
    reg_lst_p_magias[1];
    region_lst_p_obj[5];
    
//general
    cargado = false;     		//�Cargados los elementos de un menu?
    byte en_menu=0;      		//parte del menu en la q estamos, 0 = en el menu

//graficos
    g_caras=100;         		//gr�fico en el cual pondremos las caras

//PANTALLAS
    int g_lista_p_armas[3]=-1,-1,	//gr�ficos para el mapa en el que escribiremos
    					-1,-1; 	//las armas, magias y brazaletes
    int g_ranuras[1]=-1,-1;         //gr�fico donde se pondran las ranuras 
    g_lista_p_objetos[5];	    //gr�fico para las listas de los objetos
	
    armas_tot;brazaletes_tot;   //n�mero de armas y brazaletes que se tiene
    magias_tot;                 //lo mismo pero para las magias
    invoc_tot;                  //lo mismo para las invoc
    magias_tot_perso;           //las magias que tiene el perso enlazadas
    invoc_tot_perso;            //las invoc que tiene el perso enlazadas
    obj_tot[1];					//los objetos totales que hay en cada lista
    
    apoyo_arm[c_n_armas];           //ids de las armas en la lista d elas armas
    apoyo_braz[c_n_brazaletes]; //ids de los brazaletes en la lista de braz
    apoyo_mag[c_n_magias];      //ids de las magias en la lista de magias
    apoyo_invoc[c_n_invoc];     //ids de las invocaciones en la lista de invoc
    apoyo_mag_perso[6];         //ids de las magias q tiene enlazadas el perso
    apoyo_invoc_perso[6];       //ids de las invoc q tiene enlazadas el perso
	apoyo_obj[2][c_n_objetos];  //ids de los objetos en cada una de las listas


// VARIABLES DE CONTROL DE TEXTOS OCN RPG_WRITER_DELUXE
//----------------------------------------------------------------------------------
__rWDClass conf_txt;
                                                
                                                
                                                
                                                
                                                
