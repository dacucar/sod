/*------------------------------------------------------------------------------
 Shadow Of Dragon: RPG OpenSource programado en Fenix
 Copyright(C) 2003 SoD Team
 Fecha Inicio: 24/06/2002 (15:27:56h)
 Contacto: lord_danko@users.sourceforge.net
--------------------------------------------------------------------------------

 Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los 
 t�rminos de la Licencia P�blica General de GNU seg�n es publicada por la Free 
 Software Foundation, bien de la versi�n 2 de dicha Licencia o bien (seg�n su 
 elecci�n) de cualquier versi�n posterior. 

 Este programa se distribuye con la esperanza de que sea �til, pero SIN NINGUNA 
 GARANT�A, incluso sin la garant�a MERCANTIL impl�cita o sin garantizar la 
 CONVENIENCIA PARA UN PROP�SITO PARTICULAR. V�ase la Licencia P�blica General 
 de GNU para m�s detalles. 

 Deber�a haber recibido una copia de la Licencia P�blica General junto con este 
 programa. Si no ha sido as�, escriba a la Free Software Foundation, Inc., en 675 
 Mass Ave, Cambridge, MA 02139, EEUU.

--------------------------------------------------------------------------------
 Archivo: motor.inc
 Descripci�n: Procesos del motor del mundo, movimiento de los personajes, etc
 Ultima modificaci�n: 24-08-2003
------------------------------------------------------------------------------*/
import "mod_draw";
import "mod_time";
import "mod_grproc";

process Prota(pos_x,pos_y,gr�fico); //controla el gr�fico del prota
private
    UX;UY;  //guardaremos la x y la y que tenemos que pasar en cada frame
    conta;
 	p_ancho;p_alto; //guarda el alto y el ancho del perso
 	aux;aux2;aux3;
begin

    file=fpg_personajes;

    UX=pos_x;UY=pos_y;
    x=pos_x;y=pos_y;

    scroll.camera=id;
    ctype=c_scroll;

    size=100;
    graph=1;
	
	write_int(0,10,10,0,&fps);
	
    timer[1]=0;        
    loop
    	p_alto=graphic_info(file,graph,g_height);
    	p_ancho=graphic_info(file,graph,g_wide);
 
       	//si vamos pa rriba
        if(scan_code==t_arriba)
        
          	//establecemos el gr�fico adecuado
	      	if (timer[1]>15)
	            if (graph<4 OR graph>6)graph=pg_arriba[0];end
	
	            if(graph==pg_arriba[2])
	                graph = pg_arriba[0];
	        	else
	                graph=pg_arriba[2];
	            end
	            timer[1]=0;
			end		
          
          	//si no chocamos con el mapa de durezas, actualizamos el valor de uy
            colord[0]=map_get_pixel(0,g_durezas,x-p_ancho/2+4,y);  
            colord[2]=map_get_pixel(0,g_durezas,x+p_ancho/2-4,y);
            if(!(colord[0]==255 or colord[1]==255 or colord[2]==255))
            	uy-=pg_propiedades[0];
            end

          	//si no estamos m�s all� del mapa...
            if(uy<0)
               pantallas[p_actual].py+=1;
               cambia_mapa(0);
               uy=graphic_info(0,g_fase,g_height);
            end
        end

      	//si vamos pa bajo
        if(scan_code==t_abajo)
        	
        	if (timer[1]>15)
          	  //establecemos el gr�fico adecuado
            	if (graph>3)graph=pg_abajo[0];end

          		if(graph==pg_abajo[2])
                	graph = pg_abajo[0];
            	else
                	graph=pg_abajo[2];
            	end
		    	timer[1]=0;
			end
          
          	//si no chocamos ....
     		colord[0]=map_get_pixel(0,g_durezas,x-p_ancho/2+4,y+p_alto/2+2);//+graphic_info(file,graph,g_HEIGHT));
            colord[1]=map_get_pixel(0,g_durezas,x,y+p_alto/2+2);
            colord[2]=map_get_pixel(0,g_durezas,x+p_ancho/2-4,y+p_alto/2+2);
            if(!(colord[0]==255 or colord[1]==255 or colord[2]==255))
            	uy+=pg_propiedades[0];
            end
            
          	//si no estamos m�s all� del mapa...
            if(uy>graphic_info(0,g_fase,g_height))
               pantallas[p_actual].py-=1;
               cambia_mapa(0);               
               uy=0;
            end
        end

      	//si vamos pa la izquierda
        if(scan_code==t_izquierda)
	        if (timer[1]>15)
	          //establecemos el gr�fico adecuado
	            if (graph<10)graph=pg_izquierda[0];end
	
	            if(graph==pg_izquierda[2])
	                graph = pg_izquierda[0];
	            else
	                graph=pg_izquierda[2];
	            end
	            timer[1]=0;
	        end 

          	//si no chocamos ....         
            colord[0]=map_get_pixel(0,g_durezas,x-p_ancho/2,y+4);//+graphic_info(file,graph,g_HEIGHT));
            colord[1]=map_get_pixel(0,g_durezas,x-p_ancho/2,y+p_alto/4);
            colord[2]=map_get_pixel(0,g_durezas,x-p_ancho/2,y+p_alto/2-2);
            if(!(colord[0]==255 or colord[1]==255 or colord[2]==255))
            	ux-=pg_propiedades[0];
            end

          	//si no estamos m�s all� del mapa...
            if(ux<0)
               pantallas[p_actual].px-=1;
               cambia_mapa(0);               
               ux=graphic_info(0,g_fase,g_wide);
            end
        end

        //si vamos pa la derecha
        if(scan_code==t_derecha)
	        if (timer[1]>15)
	          //establecemos el gr�fico adecuado
	            if (graph<7 OR graph>9)graph=pg_derecha[0];end
	
	            if(graph==pg_derecha[2])
	                graph=pg_derecha[0];
	            else
	                graph=pg_derecha[2];
	            end
				timer[1]=0;
			end
			
          	//si no chocamos....
 			colord[0]=map_get_pixel(0,g_durezas,x+p_ancho/2,y+4);//+graphic_info(file,graph,g_HEIGHT));
            colord[1]=map_get_pixel(0,g_durezas,x+p_ancho/2,y+p_alto/4);
            colord[2]=map_get_pixel(0,g_durezas,x+p_ancho/2,y+p_alto/2-2);
            if(!(colord[0]==255 or colord[1]==255 or colord[2]==255))
            	ux+=pg_propiedades[0];
            end

          	//si no estamos m�s all� del mapa...
            if(ux>graphic_info(0,g_fase,g_wide))
               pantallas[p_actual].px+=1;
               cambia_mapa(0);               
               ux=0;
            end
        end
	  
	  	//ponemos la postura quieta del prota	
	  	if(scan_code==0)graph=(((graph-1)/3)*3+2);end
		
      	//si pulsamos la tecla del menu
        if(scan_code==t_menu)
            menu();
            signal(father,s_freeze);
            signal(id,s_freeze);
        end
      
      	//si pulsamos la tecla de batalla (temporal)
 		if(scan_code==_b)
            _fade(foff,5);
            signal(father,s_sleep);
            stop_scroll(0);
            
            //n�mero de enemigos aleatoriamente, de 1 a 6 (1-3=1;4-5=2;6=1)
 			rand_seed(time());       
            num_enemigos=rand(1,6);
            say (itoa(num_enemigos));
            if(num_enemigos==6)
            	num_enemigos=3;
            else
            	if(num_enemigos==5 || num_enemigos==4)
            		num_enemigos=1;
            	else
            		num_enemigos=2;
            	end
            end
            
          //  num_enemigos=3;
            
            //cargamos las propiedades de los enemigos
        //    rand_seed(32);
            //load(
            aux=rand(100,115);
            aux2=rand(100,115);
            aux3=rand(100,115);
            enemigos[0].gr�fico=aux;
            enemigos[0].pv=100;
            enemigos[0].pv_totales=100;
            enemigos[0].fuerza=10;
            enemigos[0].resistencia=5;
            enemigos[0].velocidad=5;
           
          //  aux=rand(100,115);
         //   rand_seed(40);
            enemigos[1].gr�fico=aux2;
            enemigos[1].pv=100;
            enemigos[1].pv_totales=100;
            enemigos[1].fuerza=10;
            enemigos[1].resistencia=5;
            enemigos[1].velocidad=5;
            
          //  aux=rand(100,115);
          //  rand_seed(2);
            enemigos[2].gr�fico=aux3;
            enemigos[2].pv=100;
            enemigos[2].pv_totales=100;
            enemigos[2].fuerza=10;
            enemigos[2].resistencia=5;
            enemigos[2].velocidad=5;
            
            iniciar_batalla(25);
            //rand(1,10);
            signal(id,s_sleep);
            while(scan_code==_b) frame;end
        end
		
		If(key(_f))efecto(5,640,480);While(scan_code==_f)Frame;End;End //esto es una prueba
        x=UX;y=UY;  //actualizamos las posiciones
        frame;
    end
end

process Personaje(pos_x,pos_y,grafico);
private
	UX;UY; //Guardo el X y el Y para el futuro que pongamos movimiento a los personajes
	nivel;
begin
	file=fpg_personajes;
 	ctype=c_scroll;

	UX=pos_x;UY=pos_y;
	x=100;y=200;

	graph=grafico;
	loop
               if (collision(TYPE Prota))
			if (KEY(_a))
				write(0,UX,UY,4,"hola");
                                frame;
			end
		end

             frame;
        end
 end
             

process cambia_mapa(mode);  //cambiar el mapa del scroll
private
       int a;
begin
    file=fpg_fase;	

	fade_off();
  //descargamos los recusos y paramos el scroll
    unload_map(0,g_fase);unload_map(0,g_durezas);
    stop_scroll(0);

    switch(mode)
        case 0:   //si es cambio de mapa de ciudad
          //cargamos el nuevo mapa
             g_fase=load_map("./map/" + pantallas[p_actual].nombre + "\" +
             		itoa(pantallas[p_actual].px)+itoa(pantallas[p_actual].py)+".map");
             g_durezas=load_map("./map/" + pantallas[p_actual].nombre + "\" +
             		itoa(pantallas[p_actual].px)+itoa(pantallas[p_actual].py)+"_d.map");
        end
        
        case 1:   //si es entrada a puerta
         /*   a=colord-15;a+=800;
            g_fase=map_clone(file,a);
            g_durezas=map_clone(fpg_durezas,a);*/
        end
    end

    start_scroll(0,0,g_fase,0,0,0);

    _fade(fon,5);
end

//----------------------
process efecto(magnitud,ancho,alto);
begin
	z=-255;
    set_center(0,graph,ancho>>1,alto>>1);
    flags=4;
    loop
    	graph=get_screen();	
        x=(ancho>>1)+rand(-magnitud,magnitud);
        y=(alto>>1)+rand(-magnitud,magnitud);
        frame;
        unload_map(0,graph);
    end
end
//---------------

