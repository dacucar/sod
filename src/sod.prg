/*------------------------------------------------------------------------------
 Shadow Of Dragon: RPG OpenSource programado en Fenix
 Copyright(C) 2003 SoD Team
 Fecha Inicio: 24/06/2002 (15:27:56h)
 Contacto: lord_danko@users.sourceforge.net
--------------------------------------------------------------------------------

 Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los 
 t�rminos de la Licencia P�blica General de GNU seg�n es publicada por la Free 
 Software Foundation, bien de la versi�n 2 de dicha Licencia o bien (seg�n su 
 elecci�n) de cualquier versi�n posterior. 

 Este programa se distribuye con la esperanza de que sea �til, pero SIN NINGUNA 
 GARANT�A, incluso sin la garant�a MERCANTIL impl�cita o sin garantizar la 
 CONVENIENCIA PARA UN PROP�SITO PARTICULAR. V�ase la Licencia P�blica General 
 de GNU para m�s detalles. 

 Deber�a haber recibido una copia de la Licencia P�blica General junto con este 
 programa. Si no ha sido as�, escriba a la Free Software Foundation, Inc., en 675 
 Mass Ave, Cambridge, MA 02139, EEUU.

--------------------------------------------------------------------------------
 Archivo: SOD.prg
 Descripci�n: Procesos principales, de carga y funciones generales
 Ultima modificaci�n: 24-08-2003
------------------------------------------------------------------------------*/


//------------------------------------------------------------------------------
                           PROGRAM Shadow_Of_Dragon;
//------------------------------------------------------------------------------
import "mod_wm";
import "mod_screen";

//import "ttf.dll";

INCLUDE "SOD.inc";

/*------------------------------------------------------------------------------
                           PROCESO PRINCIPAL
------------------------------------------------------------------------------*/
begin
    FULL_SCREEN=true;
	set_title("Shadow Of Dragon");
	set_mode(640,480,16);
    set_fps(50,0);
    dump_type=1;restore_type=1;
    Iniciar();   
end
/*------------------------------------------------------------------------------
                          FIN PROCESO PRINCIPAL
------------------------------------------------------------------------------*/
//process scan_lines()
////private
////	pointer ptr;
////	int i;
////begin           
////	loop
////			
////		for(i=0;i<240;i+=2)
////		ptr = map_buffer(1,0); 
////			memsetw(ptr+i*600,0,600);
////		end  
////		frame;
////	end
//end
//------------------------------------------------------------------------------
								process Iniciar();
//------------------------------------------------------------------------------
//Desc: Carga de recursos
begin  
  	//fpgs
    fpg_ranuras=load_fpg("./fpg/ranuras.fpg");
    fpg_menu_principal=load_fpg("./fpg/menus.fpg");
    fpg_menu=load_fpg("./fpg/menu.fpg");
    fpg_caras=load_fpg("./fpg/caras.fpg");
    fpg_titulo=load_fpg("./fpg/tsod.fpg");
    fpg_personajes=load_fpg("./fpg/person.fpg");

  	//Fuentes
    fnt_textos=load_fnt("./fnt/basica.fnt");
    fnt_numeros=load_fnt("./fnt/roja.fnt");

  	//regiones

  	//Sonidos
    sonidos[0]=load_wav("./sound/decision1.wav");
    // sonidos[1]=load_wav("../sound/intro.wav");
    // sonidos[2]=load_wav("../sound/Mal.wav");
    
  	//graficos
  	g_mensajes=load_map("./map/mensajes.map"); //gr�fico para los mensajes

    menu_principal();
    
    repeat
        frame;
    until(key(_esc))
    
    let_me_alone();
    exit();
end
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							process menu_principal();
//------------------------------------------------------------------------------
//Desc: Menu principal del juego
private
    opcion_elegida=0;
    id_musica;id_sonido;
begin
    fade_off();
    file=fpg_menu_principal;
    
    id_musica=load_song("./canciones/inicio.mid");
    id_sonido=load_wav("./sound/Decision2.wav");
    play_song(id_musica,-1);

    set_center(fpg_menu_principal, 1, 0, 0);
    put(fpg_menu_principal,1,0,0);
    //screen_put(fpg_menu_principal,1);
    
    _fade(fon,1);
    
    graph=2;x=400;y=333;
    repeat
        if (scan_code==t_arriba && y>=334)y-=43;sonido(1);
            while(scan_code==t_arriba)frame;end
        end

        if (scan_code==t_abajo && y<=418)y+=43;sonido(1);
            while(scan_code==t_abajo)frame;end
        end

        if(key(t_aceptar))
            play_wav(id_sonido,0);fade_music_off(3000);_fade(foff,4);clear_screen();
            while(key(t_aceptar))frame;end	
            //op elegida en funcion de la cordenada y
            switch (y)
                case 333:   
                    Nuevo_juego();
                end
                case 376:
                    //Cargar_juego();
                end
                default:
                    //Creditos();
                end
            end
            opcion_elegida=1;
        end
        frame;
    until(opcion_elegida<>0)
    
    unload_fpg(fpg_menu_principal);
    unload_song(id_musica);
	unload_wav(id_sonido);
end
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							process nuevo_juego(); 
//------------------------------------------------------------------------------
//Desc: Opcion del menu Nuevo juego
begin 
    iniciar_personajes();   //Cargamos los datos de los personajes
    carga_magias(".\m.smg");

#ifndef SKIP_INTRO
    intro();
#else
	iniciar_juego();
#endif

end
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							process intro();
//------------------------------------------------------------------------------
//Desc: Intro del juego
begin
    id_grafico_fondo_anim_princip=load_map("./map/pres.map");

  //m�sica de fondo
    //id_canci�n_anim_princip = load_song("..\CANCIONES\realmoff.ogg");
   // play_song(id_canci�n_anim_princip,-1);

  //Empezamos el scroll
    start_scroll(0,0,id_grafico_fondo_anim_princip,0,0,0);
    x=1000;y=1000;
    scroll.camera=id;
	
	
	_fade(fon,4);

  //movimiento y textos 
  	writer_Deluxe(&conf_txt);
    writer_deluxe(&conf_txt);
    repeat y+=5;frame; until(y>=1500);
    //mensaje(1,80,450);
    repeat x-=7;frame; until(x<=500);
    repeat y-=7;frame; until(y<=250);
  //    mensaje(2,80,450);
    repeat x+=7;frame; until(x>=1200);
 //     mensaje(3,80,450);
    repeat y+=5;frame; until(y>=1000);
   titulillo();
end
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							process Titulillo(); 
//------------------------------------------------------------------------------
//Desc: T�tulo que sale al final de la intro
private
    contador;
begin
    file = fpg_titulo;
    flags=4;
    x=320;y=240;
    timer[9]=0;
    graph=0;
    while(graph<5)
        if(timer[9]>=65) graph++;end
        frame;
    end
    flags=0;
    graph=5;
    //repeat frame; until(key(t_aceptar));
    iniciar_juego();
end
//------------------------------------------------------------------------------

//  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//                        PROCESOS DE CONTROL DEL JUEGO
//  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
process Iniciar_juego(); //controla toda la carga antes del primer nivel
private
fuente;
begin
	//apagamos la pantalla y liberamos y limpiamos recursos
	_fade(foff,4);
	
    //fade_music_off(10);//stop_song();
    stop_scroll(0);
    unload_map(0,id_grafico_fondo_anim_princip);
   	// unload_song(id_cancion_anim_princip);
    clear_screen(); //limpiado de pantalla
    SIGNAL(father,s_kill);


    frame;
    fase_1(); //COMIENZO DEL JUEGO     
  	//scan_lines();
end


//                    FASE 1(Que ya va siendo hora...)

process fase_1();
begin
    //fade(100,100,100,4);
    _fade(fon,4);

  //Intro   
    conf_txt.rFuente=load_fnt("./fnt/ariblk.fnt");//fnt_textos;
    //save_fnt(conf_txt.rFuente,"ariblk.fnt");
	/*conf_txt.rposx=70;
	conf_txt.rposy=70;
    conf_Txt.rMaxRenglones=1;
    conf_Txt.rMaxCaract=100;
    conf_Txt.rVelPto=15;		
	wd_load_text(".\Textos\fase1-1.t",1,&conf_Txt.rTexto);
	writer_deluxe(&conf_Txt);     */   
	
	_fade(foff,4);
	
  //cargas  
    g_fase=load_map("./map/" + pantallas[p_actual].nombre + "\" +
    	itoa(pantallas[p_actual].px)+itoa(pantallas[p_actual].py)+".map");
    	
    g_durezas=load_map("./map/" + pantallas[p_actual].nombre + "\" +
    	itoa(pantallas[p_actual].px)+itoa(pantallas[p_actual].py)+"_d.map");	
	
  //Empezamos el scroll
    start_scroll(0,0,g_fase,0,0,0);
    Prota(500,550,g_fase);

    _fade(fon,4);
    musica("townville.it");
    
   /* conf_txt.rposx=150;
	conf_txt.rposy=400;
	conf_Txt.rTexto="2 a�os m�s tarde";
	writer_deluxe(&conf_Txt);
    */
end
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							process _fade(tipo,vel);
//------------------------------------------------------------------------------
//Desc: Paraliza el proceso padre y hace un fundido seg�n la velocidad
begin
 	signal(father,s_freeze);
 	fade(100*tipo,100*tipo,100*tipo,vel);
 	while(fading) frame;end
 	signal(father,s_wakeup);
end
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							process musica(string archivo);
//------------------------------------------------------------------------------
//Desc: Corta la canci�n actual con un fade_music y pone una nueva
private
conta;
begin
	//apagamos la canci�n suavemente y la descargamos
	if(id_cancion>-1)
		signal(father,s_sleep);
		stop_song();
		repeat frame;until(is_playing_song()<>1)
		unload_song(id_cancion);signal(father,s_wakeup);
	end 
	//cargamos la nueva
	id_cancion=load_song("./canciones/" + archivo);
	play_song(id_cancion,-1);
end

//Reproduce un sonido de la estructura sonid
process sonido(sonid); 
begin
     play_wav(sonidos[sonid-1],0);
end
//------------------------------------------------------------------------------

process iniciar_personajes();
begin
//Provisionalmente iniciaremos la estructura personaje asi,mas tarde
//cuando ya pongamos todas las cosas que hay que iniciar meteremos esto en un
//archivo y haremos que se carge el archivo al inicio para ahorrar codigo

/*
Esto es todo provisional, hay que hacer un programa que nos permita la creaci�n
armas, magias, personajes, malos, etc y que las guarde en una estructura que m�s
adelante ser� cargada
*/
    pantallas[p_actual].nombre="townville";

    personaje.lo_tengo=true;
    personaje[1].lo_tengo=true;
    personaje[2].lo_tengo=false;
    personaje[3].lo_tengo=false;
    personaje[4].lo_tengo=false;
    personaje[5].lo_tengo=false;

    personaje.cara_x=635;
    personaje[1].cara_x=699;
    personaje[2].cara_x=763;
    personaje[3].cara_x=635;
    personaje[4].cara_x=699;
    personaje[5].cara_x=763;

    personaje.cara_y=463;
    personaje[1].cara_y=463;
    personaje[2].cara_y=463;
    personaje[3].cara_y=527;
    personaje[4].cara_y=527;
    personaje[5].cara_y=527;

    personaje[0].nombre="Danko";
    personaje[0].pv=350;
    personaje[0].pv_totales=400;
    personaje[0].pm=175;
    personaje[0].pm_totales=175;
    personaje[0].fuerza = 12;
    personaje[0].resistencia = 7;
    personaje[0].velocidad = 1000;

    personaje[1].nombre = "Ayla";
    personaje[1].pv = 256;
    personaje[1].pv_totales = 256;
    personaje[1].pm = 191;
    personaje[1].pm_totales = 191;
    personaje[1].fuerza = 7;
    personaje[1].resistencia = 6;
    personaje[1].velocidad = 1000;
    
    
    personaje[2].nombre = "Blaisse";
    personaje[2].pv = 291;
    personaje[2].pv_totales = 291;
    personaje[2].pm = 190;
    personaje[2].pm_totales = 190;
    personaje[2].fuerza = 5;

    armas[1].nombre = "Blasfemia Cortante";
    armas[1].fuerza = 3;
    armas[1].defensa = 1;
    armas[1].suerte = 0;
    armas[1].rapidez = 7;
    armas[1].peso = 3;
    armas[1].num_personaje = 0;
    armas[1].ranuras[0] = 3;
    armas[1].ranuras[1] = 1;
    armas[1].la_tengo=true;
    
    armas[2].nombre = "Filo Asesino";
    armas[2].fuerza = 18;
    armas[2].defensa = 5;
    armas[2].suerte = 6;
    armas[2].rapidez=15;
    armas[2].peso = 5;
    armas[2].num_personaje=0;
    armas[2].ranuras[0]=4;
    armas[2].ranuras[1] = 0;
    armas[2].la_tengo=true;
    
    armas[3].nombre = "Espada Celeste";
    armas[3].fuerza = 10;
    armas[3].defensa = 5;
    armas[3].suerte = 6;
    armas[3].rapidez=15;
    armas[3].peso = 5;
    armas[3].num_personaje=0;
    armas[3].ranuras[0]=5;
    armas[3].ranuras[1] = 0;
    armas[3].la_tengo=true;
    armas[4].nombre = "Nawasaki (Katana del diablo)";
    armas[4].fuerza = 10;
    armas[4].defensa = 5;
    armas[4].suerte = 6;
    armas[4].rapidez=15;
    armas[4].peso = 5;
    armas[4].num_personaje=1;
    armas[4].ranuras[0]=6;
    armas[4].ranuras[1] = 0;
    armas[4].la_tengo=true;
    armas[5].nombre = "Rumor del viento";
    armas[5].fuerza = 10;
    armas[5].defensa = 5;
    armas[5].suerte = 6;
    armas[5].rapidez=15;
    armas[5].peso = 5;
    armas[5].num_personaje=0;
    armas[5].ranuras[0]=7;
    armas[5].ranuras[1] = 0;
    armas[5].la_tengo=true;
    armas[6].nombre = "Espada Zeleste (Hacendado)";
    armas[6].fuerza = 10;
    armas[6].defensa = 5;
    armas[6].suerte = 6;
    armas[6].rapidez=15;
    armas[6].peso = 5;
    armas[6].num_personaje=0;
    armas[6].ranuras[0]=5;
    armas[6].ranuras[1] = 0;
    armas[6].la_tengo=true;

    brazaletes[1].nombre = "Garrufil Protector";
    brazaletes[1].ranuras[0] = 3;
    brazaletes[1].ranuras[1] = 0;
    brazaletes[1].lo_tengo=true;
    brazaletes[1].num_personaje=1;
    brazaletes[2].nombre = "Sueno Eterno";
    brazaletes[2].ranuras[0] = 4;
    brazaletes[2].ranuras[1] = 0;
    brazaletes[2].lo_tengo=true;
    brazaletes[2].num_personaje=2;
	brazaletes[3].nombre = "Caca flotante";
    brazaletes[3].ranuras[0] = 4;
    brazaletes[3].ranuras[1] = 0;
    brazaletes[3].lo_tengo=true;
    brazaletes[3].num_personaje=0;
	
    personaje.ident_arma[0]=1;
    personaje.ident_arma[1]=1;
    
    personaje[1].ident_arma[0]=4;
    personaje[1].ident_arma[1]=2;

    magias[1].nombre="Hechizo de luna";
    magias[1].la_tengo=true;
    magias[1].nivel=1;
    magias[1].tipo="Protecci�n";
    magias[1].gasto_pm=43;
    magias[1].fuera_batalla=Si;
    
    magias[2].nombre="Olor a pies(putrefactos)";
    magias[2].la_tengo=true;
    magias[2].nivel=1;
    magias[2].tipo="Ataque";
    magias[2].gasto_pm=123;
    magias[2].fuera_batalla=No;

    magias[3].nombre="Marsupial";
    magias[3].la_tengo=true;
    magias[3].nivel=3;
    magias[3].tipo="Curaci�n";
    magias[3].gasto_pm=22;
    magias[3].fuera_batalla=Si;

    magias[4].nombre="Fuego III";
    magias[4].la_tengo= true;
    magias[4].num_personaje=0;
    magias[4].nivel=3;
    magias[4].tipo="Ataque E.";
    magias[4].gasto_pm=255;
    magias[4].fuera_batalla=No;

    magias[5].nombre="Leti-Rap";
    magias[5].la_tengo=true;
    magias[5].nombre="Aqua";
    magias[5].nivel=1;
    magias[5].tipo="Ataque";
    magias[5].gasto_pm=98;
    magias[5].fuera_batalla=No;

    magias[6].la_tengo=true;
    magias[6].nombre="Gomina";
    magias[6].la_tengo=true;
    magias[6].nivel=5;
    magias[6].tipo="Protec & Ataque";
    magias[6].gasto_pm=589;
    magias[6].fuera_batalla=No;

    magias[7].nombre="Rayo de pikachu";
    magias[7].la_tengo= true;
    magias[7].num_personaje=0;
    magias[7].nivel=1;
    magias[7].tipo="Ataque";
    magias[7].gasto_pm=55;
    magias[7].fuera_batalla=Si;

    magias[8].nombre="Tierra II";
    magias[8].la_tengo=true;
    magias[8].nombre="Aliento";
    magias[8].nivel=1;
    magias[8].tipo="Protecci�n";
    magias[8].gasto_pm=43;
    magias[8].fuera_batalla=Si;

    magias[9].la_tengo=true;
    magias[9].nombre="Champoo";
    magias[9].la_tengo=true;
    magias[9].nivel=1;
    magias[9].tipo="Protecci�n";
    magias[9].gasto_pm=43;
    magias[9].fuera_batalla=Si;

    magias[10].nombre="Detergente";
    magias[10].la_tengo= true;
    magias[10].num_personaje=0;
    magias[10].nivel=1;
    magias[10].tipo="Protecci�n";
    magias[10].gasto_pm=43;
    magias[10].fuera_batalla=Si;
    
    invoc[1].nombre="Caballeros de la mesa cuadrada";
    invoc[1].la_tengo=true;
    invoc[2].nombre="Bahamut";
    invoc[2].la_tengo=true;
    invoc[3].nombre="Osiris";
    invoc[3].la_tengo=true;
    invoc[4].nombre="Ramhut";
    invoc[4].la_tengo= true;
    
    objetos[1].nombre = "Disparo Aturdidor";
    objetos[1].tipo = _OATAQUE;
    objetos[1].lo_tengo=true;

    objetos[2].nombre = "Bast�n de hierro";
    objetos[2].tipo = _OATAQUE;
    objetos[2].lo_tengo=true;
    
    objetos[3].nombre = "Pistola de agua";
    objetos[3].tipo = _OATAQUE;
    objetos[3].lo_tengo=true;
    
    objetos[4].nombre = "Panacea";
    objetos[4].tipo = _OCURA;
    objetos[4].lo_tengo=true;
    
    objetos[5].nombre = "Pan�cea +";
    objetos[5].tipo = _OCURA;
    objetos[5].lo_tengo=true;

    objetos[6].nombre = "Pan�cea ++";
    objetos[6].tipo = _OCURA;
    objetos[6].lo_tengo=true;

    objetos[7].nombre = "Sanador";
    objetos[7].tipo = _OCURA;
    objetos[7].lo_tengo=true;

    objetos[8].nombre = "Antipiedra";
    objetos[8].tipo = _OCURA;
    objetos[8].lo_tengo=true;

    objetos[9].nombre = "Caravaca";
    objetos[9].tipo = _OCURA;
    objetos[9].lo_tengo=true;

    objetos[10].nombre = "Despertar";
    objetos[10].tipo = _OCURA;
    objetos[10].lo_tengo=true;

    objetos[11].nombre = "Quitar maldici�n";
    objetos[11].tipo = _OCURA;
    objetos[11].lo_tengo=true;

    objetos[12].nombre = "Aire fresco";
    objetos[12].tipo = _OCURA;
    objetos[12].lo_tengo=true;

    objetos[13].nombre = "Bola de fuego";
    objetos[13].tipo = _OMAGICOS;
    objetos[13].lo_tengo=true;

    objetos[14].nombre = "Bola e fuego";
    objetos[14].tipo = _OMAGICOS;
    objetos[14].lo_tengo=true;

    objetos[15].nombre = "Despertar";
    objetos[15].tipo = _OMAGICOS;
    objetos[15].lo_tengo=true;

	objetos[16].nombre = "Quitar maldici�n";
	objetos[16].tipo = _OMAGICOS;
	objetos[16].lo_tengo=true;

	objetos[17].nombre = "Aire fresco";
	objetos[17].tipo = _OMAGICOS;
	objetos[17].lo_tengo=true;

	objetos[18].nombre = "Super-sayan II";
	objetos[18].tipo = _OMEJORA;
	objetos[18].lo_tengo=true;

	objetos[19].nombre = "LLave Ascensor";
	objetos[19].tipo = _OCLAVE;
	objetos[19].lo_tengo=true;

	objetos[20].nombre = "???";
	objetos[20].tipo = _OOTROS;
	objetos[20].lo_tengo=true;
    
end

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//Desc: Carga la informaci�n de las magias de un fichero SMG
process carga_magias(string fichero);
private
	f;i;
	auxil;
	string auxt;

begin
	f=fopen(fichero,O_READ);
	if(f>0)
	for(i=1;i<31;i++)
		fread(f,auxt);	
				
		say(auxt);
		magias[i].nombre=auxt;
		fseek(f,32,1);
		
		magias[i].la_tengo=true;
		magias[i].nivel=3;
		magias[i].tipo="Curaci�n";
		magias[i].gasto_pm=22;
		magias[i].fuera_batalla=Si;
    end
    fclose(f);
	end
end
//------------------------------------------------------------------------------
