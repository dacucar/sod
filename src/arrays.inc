/*------------------------------------------------------------------------------
 Shadow Of Dragon: RPG OpenSource programado en Fenix
 Copyright(C) 2003 SoD Team
 Fecha Inicio: 24/06/2002 (15:27:56h)
 Contacto: lord_danko@users.sourceforge.net
--------------------------------------------------------------------------------

 Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los 
 t�rminos de la Licencia P�blica General de GNU seg�n es publicada por la Free 
 Software Foundation, bien de la versi�n 2 de dicha Licencia o bien (seg�n su 
 elecci�n) de cualquier versi�n posterior. 

 Este programa se distribuye con la esperanza de que sea �til, pero SIN NINGUNA 
 GARANT�A, incluso sin la garant�a MERCANTIL impl�cita o sin garantizar la 
 CONVENIENCIA PARA UN PROP�SITO PARTICULAR. V�ase la Licencia P�blica General 
 de GNU para m�s detalles. 

 Deber�a haber recibido una copia de la Licencia P�blica General junto con este 
 programa. Si no ha sido as�, escriba a la Free Software Foundation, Inc., en 675 
 Mass Ave, Cambridge, MA 02139, EEUU.

--------------------------------------------------------------------------------
 Archivo: arrays.inc
 Descripci�n: Rutinas para tratamiento de arrays din�micos
 Ultima modificaci�n: 23-08-2003
------------------------------------------------------------------------------*/

//------------------------------------------------------------------------------
//Procesos i_dim, w_dim, b_dim
//Desc: Reservan mem�ria para X elementos de una variable tipo [x]_array.
//		Si la mem�ria ya ha sido reservada redimensionan l array y lo ponen a 0)
//------------------------------------------------------------------------------
process i_dim(i_array pointer var,int elements);
begin	
	if(!(var.st))
		var.el=alloc(elements*4);
		var.len=elements;
		var.st=1;
	else
		i_redim(var,elements);
	end
	memset(var.el,0,elements*4);
end

process w_dim(w_array pointer var,int elements);
begin
	if(!(var.st))	
		var.el=alloc(elements*2);
		var.len=elements;
		var.st=1;
	else
		w_redim(var,elements);
	end
	memset(var.el,0,elements*2);
end

process b_dim(b_array pointer var,int elements);
begin	
	if(!(var.st))
		var.el=alloc(elements);
		var.len=elements;
		var.st=1;
	else
		b_redim(var,elements);
	end
	memset(var.el,0,elements);
end
//------------------------------------------------------------------------------
//Procesos i_redim, w_redim, b_redim
//Desc: Redimensionan  una variable tipo [x]_array  para X elementos.
//		No establece a 0 (preserva los valores que el array conten�a)
//------------------------------------------------------------------------------
process i_redim(i_array pointer var, int elements);
begin
	if(var.st)
		var.el=realloc(var.el,elements*4);
		var.len=elements;
	else
		i_dim(var,elements);
	end
end

process w_redim(w_array pointer var, int elements);
begin
	if(var.st)
		var.el=realloc(var.el,elements*2);
		var.len=elements;
	else
		w_dim(var,elements);
	end
end

process b_redim(b_array pointer var, int elements);
begin
	if(var.st)
		var.el=realloc(var.el,elements);
		var.len=elements;
	else
		b_dim(var,elements);
	end
end
//------------------------------------------------------------------------------
//Procesos i_redim, w_redim, b_redim
//Desc: Liberan el espacio utilizado por una variable tipo [x]_array.
//------------------------------------------------------------------------------
process i_erase(i_array pointer var)
begin
	if(var.st)
		free(var.el);var.len=0;var.st=0;
	end
end

process w_erase(w_array pointer var)
begin
	if(var.st)
		free(var.el);var.len=0;var.st=0;
	end
end

process b_erase(b_array pointer var)
begin
	if(var.st)
		free(var.el);var.len=0;var.st=0;
	end
end
//------------------------------------------------------------------------------

