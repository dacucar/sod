/*------------------------------------------------------------------------------
 Shadow Of Dragon: RPG OpenSource programado en Fenix
 Copyright(C) 2003 SoD Team
 Fecha Inicio: 24/06/2002 (15:27:56h)
 Contacto: lord_danko@users.sourceforge.net
--------------------------------------------------------------------------------

 Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los 
 t�rminos de la Licencia P�blica General de GNU seg�n es publicada por la Free 
 Software Foundation, bien de la versi�n 2 de dicha Licencia o bien (seg�n su 
 elecci�n) de cualquier versi�n posterior. 

 Este programa se distribuye con la esperanza de que sea �til, pero SIN NINGUNA 
 GARANT�A, incluso sin la garant�a MERCANTIL impl�cita o sin garantizar la 
 CONVENIENCIA PARA UN PROP�SITO PARTICULAR. V�ase la Licencia P�blica General 
 de GNU para m�s detalles. 

 Deber�a haber recibido una copia de la Licencia P�blica General junto con este 
 programa. Si no ha sido as�, escriba a la Free Software Foundation, Inc., en 675 
 Mass Ave, Cambridge, MA 02139, EEUU.

--------------------------------------------------------------------------------
 Archivo: WD_proc.inc
 Descripci�n: motor de textos
 Ultima modificaci�n: 15-08-2003
------------------------------------------------------------------------------*/

//-------------------------------------------PROCESOS A INCLUIR-----------------------------------
process writer_deluxe(__rWDClass pointer __rConf);
private
    //variables de configuraci�n
    __rFNT,__rMaxCaract,__rMaxRenglones,__rEspaciado,__rEspacios,__rVel,__rDistY;
    __rPosx,__rPosy,__rMargenI,__rMargenS,__rVelpto,__rVelComa,__rVelPtoComa;
    string __rTXT, __rIntroChar;
    __rTotGraphics;
    
    
    
    int __rLongitud;__rLineas=1;__rNlineas;
    string __rPalabra;
    int a;j;__rPosLec;
    string __rRengl�n[__rTL]; 
                            
    int __rCoor[1];
    int __rIdtxt[__rTT];
begin
	
  	//paralizamos al padre
    SIGNAL(father,s_freeze);

	z=-256;
	
  	//inicializamos las variables con la configuraci�n pasada
  	__rFNT = __rConf.rFuente;
  	__rTXT = __rConf.rTexto;
  	__rIntroChar = __rConf.rIntroChar;
  	__rMaxCaract = __rConf.rMaxCaract;
  	__rMaxRenglones = __rConf.rMaxRenglones;
  	__rEspaciado = __rConf.rEspaciado;
  	__rEspacios = __rConf.rEspacios;
  	__rDistY = __rConf.rDistY;
  	__rVel = __rConf.rVel;
  	__rVelPto = __rConf.rVelPto;
  	__rVelComa = __rConf.rVelComa;
  	__rVelPtoComa = __rConf.rVelPtoComa;
  	__rPosx = __rConf.rPosx;
  	__rPosy = __rConf.rPosy;
  	__rMargenI = __rConf.rMargenI;
  	__rMargenS = __rConf.rMargenS;
  	__rTotGraphics = __rConf.rTotGraphics;
  	
  	//comprobaci�n de si Idtxt cuenta con los suficientes elementos
  	if((__rMaxCaract*__rMaxRenglones)>__rTT)
  		write(0,0,0,0,
  		"El valor de la costante __rTT es insuficiente. Pruebe con un valor mayor");
  		loop frame;end
  	end	
  	//comprobamos si el n�mero de renglones no supera el m�ximo establecido
  	if(__rMaxRenglones>__rTL)
  		write(0,0,0,0,
  		"El valor de rMaxRenglones no puede ser mayor a la costante __rTL. ");
  		write(0,0,10,0,
  		"Aumente el valor de dicha costante o disminuya el del total de renglones");
  		loop frame;end
  	end

  	//guardamos las coordenadas
    __rCoor[0]=__rPosx;__rCoor[1]=__rPosy;
    __rPosx+=__rMargenI;__rPosy+=__rMargenS;

  	//averiguamos cuantas l�neas ocupar� y formamos los renglones de texto
    __rLongitud=len(__rTXT);
    for(a=0;a<__rLongitud;a++)
        if(__rTXT[a]<>" ")
          	//comprobamos si no es el caracter de retorno
            if(!(__rTXT[a]==__rIntroChar))
                __rPalabra+=__rTXT[a];

              	//comprobamos si la palabra cabe en la l�nea
                if(__rMaxCaract-(j+len(__rPalabra))<=0)
                    if(__rTXT[a+1]<>" ")
                        j=0;
                        __rLineas++;
                    end
                end
             else  //si es el caracter de retorno
                 j=0;
                 __rLineas++;
             end
        else
            j+=len(__rPalabra);j++;

          	//a�adimos la palabra al rengl�n
            __rRengl�n[__rLineas-1]+=__rPalabra+" ";

            __rPalabra="";

          	//si no caben m�s caracter�s en la l�nea...
            if(__rMaxCaract-j<=0)__rLineas++;j=0;end
        end
    end
    //a�adimos la ultima palabra al rengl�n
    __rRengl�n[__rLineas-1]+=__rPalabra;

  	//comprobamos si hay m�s l�neas que renglones
    if(__rLineas<=__rMaxRenglones)__rNlineas=__rLineas;
    else __rNlineas=__rMaxRenglones;
    end

  	//ponemos el gr�fico adecuado para __rNLineas en caso de que qramos gr�ficos
    if (__rTotGraphics>0)
    	if(__rTotGraphics>__rNLineas)
        	graph=__rConf.rGraphics[__rNlineas];
    	else
        	graph=__rConf.rGraphics[0];
    	end
	end
    set_center(0,graph,0,0);
    x=__rCoor[0];y=__rCoor[1];

  	//efectos de entrada
    if (__rConf.rUseEffects>0)
  		if (__rConf.rEffects[0]>0)wd_make_effect(0);end
  		if (__rConf.rEffects[1]>0)wd_make_effect(__rConf.rEffects[1]);end
  	end
  	
  	//ahora que tenemos lo que hay que escribir, vamos a escribir :)
    repeat
        for(j=__rPosLec;j<(__rnLineas+__rPosLec);j++)
            for(a=0;a<len(__rRengl�n[j]);a++);
                // TODO: 0 can not be right...
                __rIdTXT[0]=write(__rFNT,__rPosx,__rPosy,0,__rRengl�n[j][a]);
                __rPosx+=(text_width(__rFNT,__rRengl�n[j][a])+__rEspaciado);
              	//si tenemos un espacio a�adimos el espacio adicional
                if(__rRengl�n[j][a]==" ")__rPosX+=__rEspacios;end
              	//frameamos segun las velocidades establecidas
                switch (asc(__rRengl�n[j][a-1]))
                    case asc("."):
                        frame(__rVelPto*10);
                    end
                    case asc(","):
                        frame(__rVelComa*10);
                    end
                    case asc(";"):
                        frame(__rVelPtoComa*10);
                    end
                    default:
                        frame(__rVel*10);
                    end
                end
            end
          	//pasamos a la siguiente l�nea
            __rPosx=__rCoor[0]+__rMargenI;__rPosy+=text_height(__rFNT,"D")+__rDistY;
        end
        __rPosLec+=__rNlineas;
        __rNLineas=__rLineas-__rNLineas;

        if(__rNLineas>__rMaxRenglones)__rNlineas=__rMaxRenglones;end
        
        repeat frame;until(scan_code<>__rConf.rIntroKey); //pausamos
        for(j=0;j<=__rTT;j++)delete_text(__rIdTXT[j]);end //borramos el texto
        __rPosy=__rCoor[1]+__rMargenS;
    until(__rPosLec>=__rLineas)
    
  	//efecto de salida  
    if(__rConf.rExitEffect>0)wd_exit_effect(__rConf.rExitEffect);end    
    
    SIGNAL(father,s_wakeup);

end
 
process wd_make_effect(__rEfecto);
private
    __rMiVar[1];   //para guardar info para los efectos
    i;
begin
    SIGNAL(father,s_freeze);
    SWITCH (__rEfecto)
        case 0:              //efecto translucido
           father.flags=4;
        end
        case 1:              //tama�o 0 a 100
            father.size=0;
            //FROM i=0 to 9
            for(i=0; i<=9; i++)
                father.size+=10;
                frame;
            end
        end
        case 2:              //izquierda a derecha
            __rMiVar[0]=father.x;
            father.x=-graphic_info(0,father.graph,g_wide);
            Repeat
                father.x+=60;
                if (father.x>__rMiVar[0])father.x=__rMiVar[0];end
                frame;
            Until(father.x>=__rMiVar[0])
        end
            
    end
    SIGNAL(father,s_wakeup);
end

process wd_exit_effect(__rEfecto);
private
    i;
begin
    SIGNAL(father,s_freeze);
    SWITCH(__rEfecto)
        case 1:             //tama�o 100 a 0
            //FROM i=0 to 9
            for(i=0;i<=9;i++)
                father.size-=10;
                frame;
            end
        end
        case 2:				//Desplazamiento hacia la derecha (hasta fuera de pantalla)
            Repeat
                father.x+=60;
                frame;
            Until(out_region(father,0))
        end
    end
    SIGNAL(father,s_wakeup);
end

process wd_load_text(string __rFichero, int __rLine, string pointer __rTXT);
private
    __rFile;
    a;
begin
    __rFile=fopen(__rFichero,O_read);
    if(!(__rFile==0))
        for(a=0;a<__rLine;a++)
            [__rTXT]=fgets(__rFile);
        end
        fclose(__rFile);
        [__rTXT]=substr([__rTXT],0,len([__rTXT])-1);
    else
        [__rTXT]="No se cargo el archivo correctamente";
    end
    return true;
end
//------------------------------FIN PROCESOS A INCLUIR--------------------------------	



