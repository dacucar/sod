/*------------------------------------------------------------------------------
 Shadow Of Dragon: RPG OpenSource programado en Fenix
 Copyright(C) 2003 SoD Team
 Fecha Inicio: 24/06/2002 (15:27:56h)
 Contacto: lord_danko@users.sourceforge.net
--------------------------------------------------------------------------------

 Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los 
 t�rminos de la Licencia P�blica General de GNU seg�n es publicada por la Free 
 Software Foundation, bien de la versi�n 2 de dicha Licencia o bien (seg�n su 
 elecci�n) de cualquier versi�n posterior. 

 Este programa se distribuye con la esperanza de que sea �til, pero SIN NINGUNA 
 GARANT�A, incluso sin la garant�a MERCANTIL impl�cita o sin garantizar la 
 CONVENIENCIA PARA UN PROP�SITO PARTICULAR. V�ase la Licencia P�blica General 
 de GNU para m�s detalles. 

 Deber�a haber recibido una copia de la Licencia P�blica General junto con este 
 programa. Si no ha sido as�, escriba a la Free Software Foundation, Inc., en 675 
 Mass Ave, Cambridge, MA 02139, EEUU.

--------------------------------------------------------------------------------
 Archivo: bat.inc
 Descripci�n: Motor de batallas
 Ultima modificaci�n: 15-08-2003
------------------------------------------------------------------------------*/


//------------------------------------------------------------------------------
							process iniciar_batalla(gfondo); 
//------------------------------------------------------------------------------
//Desc: Carga el fpg de las batallas, pone el menu, lo controla y otras tareas
Private
	conta;
Begin       
	musica("./batallas/battle1.it");
	//cargar las fuentes
	fnt_act1=load_fnt("./fnt/bat_barras.fnt");
	
	//abrir el fondo y ponerlo, cargar el fpg batallas
	fpg_batallas=load_fpg("./fpg/batallas.fpg");
	g_mbatalla=map_clone(fpg_batallas,1);
	g_fbatalla=load_map("./map/battle/BG" + itoa(gfondo) +".map");
	set_center(0,g_fbatalla,0,0);
	put(0,g_fbatalla,0,0);
	
	file=fpg_batallas;
	
	//creamos los enemigos
	For(conta=0;conta<num_enemigos;conta++)
		crea_enemigo(conta);
	End
	//barritas de fonde de vida y tiempo 
	set_center(fpg_batallas,3,graphic_info(fpg_batallas,3,g_wide),
			graphic_info(fpg_batallas,3,g_center_y));
	For(conta=0;conta<protas_b[0];conta++)
	//	map_put(fpg_batallas,1,3,623,30+conta*30);	//b_energ�a en men�
		put(fpg_batallas,6,500,(conta+1)*18); 		//timer   
		crea_prota(protas_b[conta+1],conta);
	End

	_fade(fon,5);
	controla_batalla();
End
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							process controla_batalla();
//------------------------------------------------------------------------------
//Desc: Controla del input del men�
Private
	_seccion; 		//�donde estamos?
	enemigo_sel; 	//enemigo seleccionado
	num_prota=-1;	//personaje que ataca
Begin
	z=_z1;
	file=fpg_batallas;
	
	Loop
		Repeat num_prota=hay_alguien(); Frame; Until(num_prota<>-1); //haya alguien pa atacar
		grafico_menu_batalla(num_prota);
		Repeat Frame; Until(menu_b==true)	//menu subido del todo
		graph=2;flags=2;x=83;y=381;_seccion=0;
		Loop			
			//Si pulsamos la tecla aceptar
			If(key(t_aceptar))
				Switch (_seccion)
					Case 0: 	//ataque normal
						x=enemigos[0].x;y=enemigos[0].y; _seccion=10;	
					End
					Case 1: 	//defensa
					End
					Case 2:		//magia
					End	
					Case 3:		//invocaci�n
					End
					Case 4:		//objeto
					End
					Case 10:	//ataque al enemigo
						haya_atacado[num_prota]=true;
						menu_b=false;
						Break;
					End
				End
				While(key(t_aceptar))Frame;End
			End
			
			//si pulsamos la tecla derecha
			If(key(t_derecha)) 
				Switch (_seccion)
					Case 0 .. 3: 	//Eligiendo tipo de ataque
						x+=78;
						_seccion++;	
					End
					Case 10:		//si estamos eligiendo enemigo
						If((num_enemigos-1)>enemigo_sel)
							enemigo_sel++;
							x=enemigos[enemigo_sel].x;y=enemigos[enemigo_sel].y;
						End
					End
				End
				While(key(t_derecha))Frame;End
			End
			
			//si pulsamos la tecla izquierda
			If(key(t_izquierda)) 
				Switch (_seccion)
					Case 1 .. 4: 	//Eligiendo tipo de ataque
						x-=78;
						_seccion--;	
					End
					Case 10:		//si estamos eligiendo enemigo
						If(enemigo_sel>0)
							enemigo_sel--;
							x=enemigos[enemigo_sel].x;y=enemigos[enemigo_sel].y;
						End
					End
				End
				While(key(t_izquierda))Frame;End
			End
			
			//si pulsamos la tecla abajo
			If(key(t_abajo)) 
				Switch (_seccion)
					Case 10:	//si estamos eligiendo enemigo
						//pasamos a seleccionar los protas como enemigos
					End
				End
				While(key(t_derecha))Frame;End
			End
			
			//si pulsamos la tecla cancelar
			If(key(t_cancelar)) 
				Switch (_seccion)
					Case 10:	//si estamos eligiendo enemigo
						x=83;y=381;_seccion=0;
					End
				End
				While(key(t_izquierda))Frame;End
			End
			
			Frame;
		End
		Frame;
	End
End
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							Process crea_prota(num_prota,orden);
//------------------------------------------------------------------------------
//Desc: controla el temporizador de los buenos 
Private
	ancho_reg;
	pointer vel;
	conta;
Begin
	b_energia(orden);	//fondo de las barritas
	b_vida(num_prota,orden);
	b_pm(num_prota,orden);
	
	file=fpg_batallas;
	graph=7;x=500;y=18*(orden+1);region=17+orden;
	
	vel=&personaje[num_prota].velocidad;
	Loop
		timer[orden+1]=0;
		//regi�n del timer
		Repeat
			ancho_reg=timer[orden+1]*138/[vel];
			define_region(17+orden,500,18*(orden+1),ancho_reg,12);
			Frame;
		Until(timer[orden+1]>[vel])
		define_region(17+orden,500,18*(orden+1),138,12); //tiempo completado
		
		//buscamos un hueco en el orden de los protas y metemos al prota
		for(conta=0;conta<protas_b[0];conta++)
			if(orden_protas[conta]==-1)
				orden_protas[conta]=num_prota;
				break;
			end
		end
		
		repeat frame;until(haya_atacado[num_prota]) //esperamos a atacar
		
		//desplazamos los turnos hacia la izquierda
		for(conta=0;conta<protas_b[0]-1;conta++)
			orden_protas[conta]=orden_protas[conta+1];
		end
		orden_protas[protas_b[0]-1]=-1;
		
		haya_atacado[num_prota]=false;
		Frame;
	End
	
End
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							Process b_energia(orden); 
//------------------------------------------------------------------------------
//Desc: Fondo para la barra de energia (de vida y pm)
Begin
	file=fpg_batallas;graph=3;x=623;y=381+(30*orden);
	Loop Frame;End
End
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							Process b_vida(num_prota,orden);
//------------------------------------------------------------------------------
//Desc: Este proceso controla la barra de vida de un num_prota y la muestra
//		haciendo uso de las regiones 20,21 o 22 seg�n el orden 
Private 
	m;  //mapa clonado
	ancho_reg;
	last_pv;
	String vida;
	id_txtvida; //id del texto
	Byte fnt_critica; //saber si estaba la fuente cr�tica
	color_azul; //Pa no repetirlo tantas veces
	Pointer _pv;
	int _pvtot; //m�s c�modo...
Begin
	z=-256;file=fpg_batallas;
	
	m=map_clone(fpg_batallas,4); //barrita de vida
	set_center(0,m,0,0);
	graph=m;x=475;y=377+(30*(orden));
	
	color_azul=rgb(148,137,213);Set_text_color(color_azul);
	id_txtvida=write_string(fnt_act1,545,380+(30*orden),2,&vida); //muestra la vida
	
	//Guardamos las variables ak�n pa q sea m�s facil acceder a la vida
	_pv=&personaje[num_prota].pv;
	say([_pv]);
	_pvtot=personaje[num_prota].pv_totales;
//	say(_pvtot*0.1);

	Loop 
		//Si el personaje ha cambiado de vida desde la �ltima vez...
		If(last_pv<>[_pv])
			//actualizamos la regi�n
			ancho_reg=[_pv]*72/_pvtot;
			define_region(20+orden,475,377+30*orden,ancho_reg,8);
			region=20+orden;
			//actualizamos el texto de la vida
			vida=itoa([_pv]) + " de " + itoa(_pvtot);
			last_pv=[_pv];
		End
		 //si la vida es menor que el 10% ponemos la fuente amarilla 
		If(([_pv]*(10)<_pvtot) && 
			(fnt_critica==false))
			Set_text_color(rgb(253,253,0)); //color amarillo
			delete_text(id_txtvida);
			id_txtvida=write_string(fnt_act1,545,380+(30*orden),2,&vida);
			fnt_critica=true;
		Else //si no, comprobamos si la letra era amarilla o no
			If(fnt_critica==true && !(_pvtot*0.1>[_pv]))
				Set_text_color(color_azul); 
				delete_text(id_txtvida);
				id_txtvida=write_string(fnt_act1,545,380+(30*orden),2,&vida);
				fnt_critica=false;
			End
		End
		Frame;
	End
End
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							Process b_pm(num_prota,orden);
//------------------------------------------------------------------------------
//Desc: Este proceso controla la barra de m�gia de un num_prota y la muestra
//		haciendo uso de las regiones 23,24 o 25 seg�n el orden
Private 
	m; //mapa clonado;
	ancho_reg;
	last_pm;
	String magia;
	id_txtmagia;
	Byte fnt_critica; //saber si estaba la fuente cr�tica
	color_azul; //Pa no repetirlo tantas veces 
	Pointer _pm;
	_pmtot;
	
Begin
	z=-256;file=fpg_batallas;
	m=map_clone(fpg_batallas,5); //mapa de la barrita de vida
	graph=m;x=586;y=381+(30*(orden));
	
	color_azul=rgb(148,137,213);Set_text_color(color_azul);
	id_txtmagia=write_string(fnt_act1,560,380+(30*orden),0,&magia); //muestra la vida
	
	//Guardamos las variables ak�n pa q sea m�s facil acceder a las magias
	_pm=&personaje[num_prota].pm;
	_pmtot=personaje[num_prota].pm_totales;
	
	Loop 
		//Si el personaje ha cambiado de pm desde la �ltima vez 
		If(last_pm<>[_pm])
			//actualizamos la regi�n
			ancho_reg=[_pm]*72/_pmtot;
			define_region(23+orden,547+75-ancho_reg,377+30*orden,ancho_reg,8);
			region=23+orden;
			//actualizamos el texto de la magia
			magia=itoa([_pm]) + " de " + itoa(_pmtot);
			last_pm=[_pm];
		End
		//si la magia es menor que el 10% ponemos la fuente amarilla 
		If((_pmtot*0.1>[_pm]) && 
			(fnt_critica==false))
			Set_text_color(rgb(253,253,0)); //color amarillo
			delete_text(id_txtmagia);
			id_txtmagia=write_string(fnt_act1,560,380+(30*orden),0,&magia);
			fnt_critica=true;
		Else //si no, comprobamos si la letra era amarilla o no
			If(fnt_critica==true && !(_pmtot*0.1>[_pm]))
				Set_text_color(color_azul); 
				delete_text(id_txtmagia);
				id_txtmagia=write_string(fnt_act1,560,380+(30*orden),0,&magia);
				fnt_critica=false;
			End
		End
		Frame;
	End
End
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							Process grafico_menu_batalla(num_prota);
//------------------------------------------------------------------------------
//Desc: Pone el gr�fico del menu con los dibujos del prota pasado
Private
	conta; 
	ancho; alto;
Begin	
	z=_z2;
	file=fpg_batallas;x=0;
	graph=1;
	ancho=graphic_info(file,graph,G_WIDE);
	alto=graphic_info(file,graph,G_HEIGHT);
    
    //restablecemos el menu
    memcopy(map_buffer(file,1),map_buffer(0,g_mbatalla),ancho*alto*2);
    
    //ponemos los dibujos de acciones
    For(conta=0;conta<4;conta++)
    	map_put(file,1,10+4*num_prota+conta,83+78*conta,61);
    End
    map_put(file,1,8,395,61);	//mochilita
    
    //animaci�n de subida
    for(y=480+alto;y>=480;y-=4)
    //From y=480+alto To 480 Step -4
    	//y-=4;
    	Frame;
    End 
    menu_b=true;
    Repeat Frame;Until(menu_b==false)
	
	//animaci�n de bajada
    //From y=480 To y=480+alto Step 4
    for(y=480;y>=480+alto;y+=4)
    	//y+=4;
    	Frame;
    End     
End
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							Process crea_enemigo(num_enemigo);
//------------------------------------------------------------------------------
//Desc: Crea y controla los temporizadores de los enemigos y su gr�fico
Begin
	z=_z2;
	file=fpg_batallas;
	graph=enemigos[num_enemigo].gr�fico;
    //centro de la imagen en la parte central inferior
	set_center(0,graph,graphic_info(0,graph,g_wide)/2,
				graphic_info(0,graph,g_height)); 
				
	Switch (num_enemigos)
		Case 1:	//un enemigo
			x=320;y=230;
		End
		Case 2: //dos enemigos
			If(num_enemigo==0)x=180;y=270; //si somos el enemigo 1
			Else x=460;y=270; //si somos el 2
			End
		End
		Case 3:
			If(num_enemigo==0)x=120;y=260; //si somos el enemigo 1
			Else If(num_enemigo==1)x=320;y=230; //si somos el 2
				 Else x=520;y=260;
				 End
			End
		End
	End	
	//guardamos sus posiciones
	enemigos[num_enemigo].x=x;enemigos[num_enemigo].y=y;
	
	timer[9]=0;timer[num_enemigo+4]=0;			 
	Loop 
    	//animaci�n
    	If(timer[9]>=rand(25,150))flags=abs(flags-1);timer[9]=0;End 
		Frame; 
	End
End
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
							Process hay_alguien(); 
//------------------------------------------------------------------------------
//Desc: busca si hay alguien en la lista de los protas y devuelve su id
Private
	conta;
Begin
	For(conta=0;conta<protas_b[0];conta++)
		If(orden_protas[conta]<>-1)
			Return orden_protas[conta];
		End
	End
	Return -1;
End
//------------------------------------------------------------------------------

