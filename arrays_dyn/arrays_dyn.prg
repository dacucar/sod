Import "mod_text"
Import "mod_video"
Import "mod_key"
Import "mod_mem"


Type i_array
	pointer el;
	int len;
	int st=0;
End


Type b_array
	pointer el;
	int len;
	int st=0;
End


Type w_array
	pointer el;
	int len;
	int st=0;
End


Private
	i_array mi_array;
End
Begin
	set_mode(320,240,32);

	// creamos un array de 4 elementos
	i_dim(&mi_array,4);	
	mi_array.el[0]=write(0,0,0,0, "Primer elmento del array");
	mi_array.el[1]=write(0,0,10,0, "Segundo elemento del array");
	mi_array.el[2]=write(0,0,20,0, "Tercer elemento del array");
	mi_array.el[3]=write(0,0,30,0,"Cuarto elemento del array");

	Repeat Frame; Until(key(_ENTER))

	borra_textos(mi_array);

	// redimensionamos a 2 elementos y inicializamos a 0
	i_dim(&mi_array,2);	
	mi_array.el[0]=write(0,0,0,0, "He borrado todo el texto");
	mi_array.el[1]=write(0,0,10,0, "sin saber cuanto texto hab�a :D");

	Repeat Frame; Until(key(_ENTER))

	// liberamos
	i_erase(mi_array);	
End


Function borra_textos(i_array mi_array)
Private conta;	
Begin
	For(conta=0;conta<mi_array.len;conta++)
		delete_text(mi_array.el[conta]);
	End
End


Function i_dim(i_array pointer var,int elements);
Begin	
	if(!(var.st))
		var.el=alloc(elements*4);
		var.len=elements;
		var.st=1;
	else
		i_redim(var,elements);
	End
	memset(var.el,0,elements*4);
End


Function w_dim(w_array pointer var,int elements);
Begin
	if(!(var.st))	
		var.el=alloc(elements*2);
		var.len=elements;
		var.st=1;
	else
		w_redim(var,elements);
	End
	memset(var.el,0,elements*2);
End


Function b_dim(b_array pointer var,int elements);
Begin	
	if(!(var.st))
		var.el=alloc(elements);
		var.len=elements;
		var.st=1;
	else
		b_redim(var,elements);
	End
	memset(var.el,0,elements);
End


Function i_redim(i_array pointer var, int elements);
Begin
	var.el=realloc(var.el,elements*4);
	var.len=elements;
End


Function w_redim(w_array pointer var, int elements);
Begin
	var.el=realloc(var.el,elements*2);
	var.len=elements;
End


Function b_redim(b_array pointer var, int elements);
Begin
	var.el=realloc(var.el,elements);
	var.len=elements;
End


Function i_erase(i_array pointer var)
Begin
	free(var.el);var.len=0;var.st=0;
End


Function w_erase(w_array pointer var)
Begin
	free(var.el);var.len=0;var.st=0;
End


Function b_erase(b_array pointer var)
Begin
	free(var.el);var.len=0;var.st=0;
End

