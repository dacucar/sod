# Shadow Of Dragon

An old, unfinished, 2D RPG game project, dating from 2003 and written in [Fenix][fenix]
and then ported to [BennuGD][bennugd] just for the nostalgia of seeing it running again.

I was 15 years old at the time and had lot of fun programming for the fun of
it. The game was never completed nor had a clear story.

I was heavily inspired by Final Fantasy VII and one of the ambitions was to have
a concept in which the characters could carry _weapons_ and each of them had
a number of _slots_ to assign _magic items and invocations_ to be used
within battles.

## Building & Running the game

You'll need:

* Python & pipenv.
* BennuGD.

The python requirement is because I use [SCons](https://scons.org/doc/production/HTML/scons-user.html) 
as build system for BennuGD projects. If you know what you are doing you don't really it, just build
`sod.prg`.

```
pipenv install
```

To run the game:

```
cd src/
bennugd sod.dcb
```

## Downloading BennuGD

You can [download](https://www.bennugd.org/downloadss) windows binaries for BennuGD from the
official BennGD site.

The Linux binaries won't work on modern Linux distros so the alternatives are:

* Build it from sources.
* Use [bennugd-flatpak](https://gitlab.com/dacucar/bennugd-flatpak).

## Keyboard Controls

General:

* Arrow keys to move.
* Enter to accept.
* Del key to cancel. E.g.: going to the previous menu.

In the game:

* `m` to show the menu.
* `b` to initiate the battle mode (you can't exit it though).

## Screenshots

![screenshot 1](sshot-1.png)


[fenix]: https://en.wikipedia.org/wiki/Fenix_Project
[bennugd]: https://www.bennugd.org
