Attribute VB_Name = "modSOD"
Option Explicit

Type magia
     nulo As Integer  'espacio para el varspace
     nombre As String
     Nivel As Integer
     GastoPM As Integer
     Vida As Long
     PMagia As Long
     Suerte As Long
     Velocidad As Long
     Fuerza As Long
     Resistencia As Long
     flag_estado As Byte
     id_personaje As Byte
     fuera_batalla As Byte
     la_tengo As Byte
End Type

Public Sub guardar_mag(archivo As String, mag() As magia)
    Dim n As Integer, lon As Long, i As Integer
    n = FreeFile()
    
    Open archivo For Binary Access Write As #n
    'Escribir los datos de la estructura
    Put #n, , mag()
    
    'modificar los varspace
    Seek #n, 1
    For i = 0 To 29
        lon = Len(mag(i).nombre)
        Put #n, , lon
        Seek #n, Seek(n) + lon + 32
        Debug.Print Seek(n)
    Next
    
    'Cerrar el archivo
    Close #n
End Sub

Public Sub abrir_mag(archivo As String, mag() As magia)
    Dim n As Integer, aux As Integer, nul As Integer, i As Integer
    n = FreeFile()
    
    'modificamos los varspace
    Open archivo For Binary Access Read Write As #n
    For i = 0 To 29
        Get #n, , aux
        Seek #n, Seek(n) - 2
        Put #n, , nul
        Put #n, , aux
        Seek #n, Seek(n) + aux + 32
    Next
    Close #n
    
    Open archivo For Binary Access Read As #n
    
    'obtenerlos datos de la estructura
    Get #n, , mag
    'Cerrar el archivo
    Close #n
    
    'Restaurar los varspace
    guardar_mag archivo, mag
    
    MsgBox "Archivo abierto", vbInformation, "SODTool"
End Sub
