VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMagias 
   BackColor       =   &H8000000B&
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Editor de magias"
   ClientHeight    =   5100
   ClientLeft      =   165
   ClientTop       =   690
   ClientWidth     =   7305
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5100
   ScaleWidth      =   7305
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog cd 
      Left            =   5640
      Top             =   2040
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      Filter          =   "Archivos de magias SOD|*.smg"
   End
   Begin VB.TextBox txtGastoPM 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2760
      TabIndex        =   27
      Text            =   "0"
      Top             =   840
      Width           =   1095
   End
   Begin VB.CheckBox opFBatalla 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000A&
      Caption         =   "�Uso fuera de batalla?"
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   240
      TabIndex        =   26
      Top             =   4680
      Width           =   1935
   End
   Begin VB.CheckBox opLaTengo 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H8000000A&
      Caption         =   "La tengo"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3000
      TabIndex        =   25
      Top             =   4680
      Width           =   975
   End
   Begin VB.TextBox txtNivel 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   960
      TabIndex        =   23
      Text            =   "0"
      Top             =   840
      Width           =   495
   End
   Begin VB.ComboBox cmbEnlazada 
      Appearance      =   0  'Flat
      Height          =   315
      ItemData        =   "frmMagias.frx":0000
      Left            =   1440
      List            =   "frmMagias.frx":0019
      Style           =   2  'Dropdown List
      TabIndex        =   22
      Top             =   4080
      Width           =   2055
   End
   Begin VB.TextBox txtNombre 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      MaxLength       =   25
      TabIndex        =   20
      Top             =   240
      Width           =   2895
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000A&
      Caption         =   "Propiedades"
      ForeColor       =   &H80000008&
      Height          =   2535
      Left            =   120
      TabIndex        =   2
      Top             =   1440
      Width           =   3855
      Begin VB.CheckBox opDormido 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000A&
         Caption         =   "Dormido"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   720
         TabIndex        =   19
         Top             =   2160
         Width           =   975
      End
      Begin VB.CheckBox opMuerto 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000A&
         Caption         =   "Muerto"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2040
         TabIndex        =   18
         Top             =   2160
         Width           =   855
      End
      Begin VB.CheckBox opEnvenenado 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000A&
         Caption         =   "Envenenado"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2040
         TabIndex        =   17
         Top             =   1800
         Width           =   1335
      End
      Begin VB.CheckBox opNormal 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000A&
         Caption         =   "Normal"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   720
         TabIndex        =   16
         Top             =   1800
         Width           =   855
      End
      Begin VB.TextBox txtResistencia 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3120
         TabIndex        =   15
         Text            =   "9999"
         Top             =   1080
         Width           =   495
      End
      Begin VB.TextBox txtFuerza 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3120
         TabIndex        =   14
         Text            =   "9999"
         Top             =   660
         Width           =   495
      End
      Begin VB.TextBox txtVel 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3120
         TabIndex        =   13
         Text            =   "9999"
         Top             =   240
         Width           =   495
      End
      Begin VB.TextBox txtSuerte 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   12
         Text            =   "9999"
         Top             =   1080
         Width           =   495
      End
      Begin VB.TextBox txtPMagia 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   11
         Text            =   "9999"
         Top             =   660
         Width           =   495
      End
      Begin VB.TextBox txtVida 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   10
         Text            =   "9999"
         Top             =   240
         Width           =   495
      End
      Begin VB.Line Line2 
         X1              =   120
         X2              =   3720
         Y1              =   1440
         Y2              =   1440
      End
      Begin VB.Line Line1 
         X1              =   1800
         X2              =   1800
         Y1              =   120
         Y2              =   1320
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Suerte:"
         Height          =   195
         Left            =   330
         TabIndex        =   9
         Top             =   1080
         Width           =   510
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "P. M�gia:"
         Height          =   195
         Left            =   165
         TabIndex        =   8
         Top             =   660
         Width           =   675
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Vida:"
         Height          =   195
         Left            =   480
         TabIndex        =   7
         Top             =   240
         Width           =   360
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Pone estados:"
         Height          =   195
         Left            =   240
         TabIndex        =   6
         Top             =   1560
         Width           =   1020
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Velocidad:"
         Height          =   195
         Left            =   2160
         TabIndex        =   5
         Top             =   240
         Width           =   750
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Resistencia:"
         Height          =   195
         Left            =   2040
         TabIndex        =   4
         Top             =   1080
         Width           =   870
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Fuerza:"
         Height          =   195
         Left            =   2385
         TabIndex        =   3
         Top             =   660
         Width           =   525
      End
   End
   Begin VB.ListBox lstMagias 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000A&
      Height          =   4710
      Left            =   4200
      TabIndex        =   0
      Top             =   120
      Width           =   3015
   End
   Begin VB.Line Line7 
      X1              =   240
      X2              =   3960
      Y1              =   5040
      Y2              =   5040
   End
   Begin VB.Line Line6 
      X1              =   240
      X2              =   3960
      Y1              =   1320
      Y2              =   1320
   End
   Begin VB.Line Line5 
      X1              =   240
      X2              =   3960
      Y1              =   720
      Y2              =   720
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      Caption         =   "Gasto P.M."
      Height          =   195
      Left            =   1800
      TabIndex        =   28
      Top             =   840
      Width           =   795
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      Caption         =   "Nivel:"
      Height          =   195
      Left            =   480
      TabIndex        =   24
      Top             =   840
      Width           =   405
   End
   Begin VB.Line Line4 
      X1              =   240
      X2              =   3960
      Y1              =   120
      Y2              =   120
   End
   Begin VB.Line Line3 
      X1              =   240
      X2              =   3960
      Y1              =   4560
      Y2              =   4560
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Caption         =   "Enlazada a:"
      Height          =   195
      Left            =   360
      TabIndex        =   21
      Top             =   4080
      Width           =   840
   End
   Begin VB.Label label1 
      AutoSize        =   -1  'True
      Caption         =   "Nombre:"
      Height          =   195
      Left            =   360
      TabIndex        =   1
      Top             =   240
      Width           =   600
   End
   Begin VB.Menu mnuArchivo 
      Caption         =   "Archivo"
      Begin VB.Menu sbnuCargar 
         Caption         =   "Cargar..."
      End
      Begin VB.Menu sbnuGuardar 
         Caption         =   "Guardar"
      End
      Begin VB.Menu sbnuGuardarComo 
         Caption         =   "Guardar como..."
      End
   End
End
Attribute VB_Name = "frmMagias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const c_n_magias = 29
Dim mag(c_n_magias) As magia
Dim magia_sel As Integer 'La m�gia que hay seleccionada
Dim archivo As String
Private Sub txt_with_matrix(i As Integer) 'rellena los textbox con los datos de la matriz
    txtNombre = mag(i).nombre
    opFBatalla.Value = mag(i).fuera_batalla
    txtFuerza = mag(i).Fuerza
    txtGastoPM = mag(i).GastoPM
    cmbEnlazada.ListIndex = mag(i).id_personaje
    opLaTengo.Value = mag(i).la_tengo
    txtNivel = mag(i).Nivel
    txtPMagia = mag(i).PMagia
    txtResistencia = mag(i).Resistencia
    txtSuerte = mag(i).Suerte
    txtVel = mag(i).Velocidad
    txtVida = mag(i).Vida
    
    If (mag(i).flag_estado And 1) = 1 Then opNormal.Value = 1 Else opNormal.Value = 0
    If (mag(i).flag_estado And 2) = 2 Then opEnvenenado.Value = 1 Else opEnvenenado.Value = 0
    If (mag(i).flag_estado And 4) = 4 Then opDormido.Value = 1 Else opDormido.Value = 0
    If (mag(i).flag_estado And 8) = 8 Then opMuerto.Value = 1 Else opMuerto.Value = 0
End Sub
Private Sub Randomize_matrix(i As Integer)  'para que se rellene la matriz
        mag(i).nombre = "Magia" & i + 1
        mag(i).flag_estado = 0
        mag(i).fuera_batalla = 0
        mag(i).Fuerza = Int(1255) * Rnd
        mag(i).GastoPM = Int(1355) * Rnd
        mag(i).id_personaje = Int(6) * Rnd
        mag(i).la_tengo = 0
        mag(i).Nivel = Int(9999) * Rnd
        mag(i).PMagia = Int(100) * Rnd
        mag(i).Resistencia = Int(100) * Rnd
        mag(i).Suerte = Int(100) * Rnd
        mag(i).Velocidad = Int(100) * Rnd
        mag(i).Vida = Int(9999) * Rnd
End Sub
Private Sub refresh_lstMagias() 'Actualiza la lista con los nombres de las magias
        Dim i As Integer
        For i = 0 To c_n_magias
            lstMagias.List(i) = mag(i).nombre
        Next
End Sub

Private Sub cmbEnlazada_Click()
    mag(magia_sel).id_personaje = cmbEnlazada.ListIndex
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Randomize
    For i = 0 To c_n_magias
        lstMagias.AddItem "Magia" & i + 1
        Randomize_matrix (i)
    Next
    txt_with_matrix 0
End Sub

Private Sub lstMagias_Click()
    magia_sel = lstMagias.ListIndex
    txt_with_matrix magia_sel
End Sub

Private Sub opDormido_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If opDormido.Value = 1 Then
        mag(magia_sel).flag_estado = mag(magia_sel).flag_estado + 4
    Else
        If Not mag(magia_sel).flag_estado = 0 Then
            mag(magia_sel).flag_estado = mag(magia_sel).flag_estado - 4
        End If
    End If
End Sub

Private Sub opEnvenenado_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If opEnvenenado.Value = 1 Then
        mag(magia_sel).flag_estado = mag(magia_sel).flag_estado + 2
    Else
        If Not mag(magia_sel).flag_estado = 0 Then
            mag(magia_sel).flag_estado = mag(magia_sel).flag_estado - 2
        End If
    End If
End Sub

Private Sub opFBatalla_Click()
    mag(magia_sel).fuera_batalla = opFBatalla.Value
End Sub

Private Sub opLaTengo_Click()
    mag(magia_sel).la_tengo = opLaTengo.Value
End Sub

Private Sub opMuerto_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If opMuerto.Value = 1 Then
        mag(magia_sel).flag_estado = mag(magia_sel).flag_estado + 8
    Else
        If Not mag(magia_sel).flag_estado = 0 Then
            mag(magia_sel).flag_estado = mag(magia_sel).flag_estado - 8
        End If
    End If
End Sub

Private Sub opNormal_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If opNormal.Value = 1 Then
        mag(magia_sel).flag_estado = mag(magia_sel).flag_estado + 1
    Else
        If Not mag(magia_sel).flag_estado = 0 Then
            mag(magia_sel).flag_estado = mag(magia_sel).flag_estado - 1
        End If
    End If
End Sub

Private Sub sbnuCargar_Click()
    On Error GoTo errores
    
    cd.DialogTitle = "Abrir archivo de magias"
    cd.ShowOpen
    
    modSOD.abrir_mag cd.FileName, mag()
    archivo = cd.FileName
    txt_with_matrix 0
    refresh_lstMagias

errores:
    If Err.Number = cdlCancel Then Exit Sub
End Sub

Private Sub sbnuGuardar_Click()
    If Len(archivo) > 1 Then
        modSOD.guardar_mag archivo, mag()
        MsgBox "Archivo guardado", vbInformation, "SODTool"
    Else
        sbnuGuardarComo_Click
    End If
End Sub

Private Sub sbnuGuardarComo_Click()
    On Error GoTo errores
    cd.DialogTitle = "Guardar Archivo de magias"
    cd.ShowSave
    modSOD.guardar_mag cd.FileName, mag()
    archivo = cd.FileName
    MsgBox "Archivo guardado", vbInformation, "SODTool"
errores:
    If Err.Number = cdlCancel Then Exit Sub
End Sub

Private Sub txtFuerza_LostFocus()
    mag(magia_sel).Fuerza = CInt(txtFuerza)
End Sub

Private Sub txtGastoPM_LostFocus()
    mag(magia_sel).GastoPM = CInt(txtGastoPM)
End Sub

Private Sub txtNivel_LostFocus()
    mag(magia_sel).Nivel = CInt(txtNivel)
End Sub



Private Sub txtNombre_LostFocus()
    mag(magia_sel).nombre = txtNombre
    lstMagias.List(magia_sel) = txtNombre
End Sub


Private Sub txtPMagia_LostFocus()
    mag(magia_sel).PMagia = CInt(txtPMagia)
End Sub

Private Sub txtResistencia_LostFocus()
    mag(magia_sel).Resistencia = CInt(txtResistencia)
End Sub

Private Sub txtSuerte_LostFocus()
    mag(magia_sel).Suerte = CInt(txtSuerte)
End Sub

Private Sub txtVel_LostFocus()
    mag(magia_sel).Velocidad = CInt(txtVel)
End Sub

Private Sub txtVida_LostFocus()
    mag(magia_sel).Vida = CInt(txtVida)
End Sub
