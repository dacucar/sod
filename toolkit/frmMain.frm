VERSION 5.00
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SOD TOOLKIT"
   ClientHeight    =   7170
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9585
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   Picture         =   "frmMain.frx":0000
   ScaleHeight     =   7170
   ScaleWidth      =   9585
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H8000000E&
      Height          =   2175
      Left            =   2640
      TabIndex        =   0
      Top             =   4560
      Width           =   4215
      Begin VB.CommandButton btInvoc 
         Caption         =   "Invoc."
         Height          =   375
         Left            =   240
         TabIndex        =   7
         Top             =   720
         Width           =   975
      End
      Begin VB.CommandButton btTextos 
         Caption         =   "Textos"
         Height          =   375
         Left            =   3120
         TabIndex        =   6
         Top             =   120
         Width           =   975
      End
      Begin VB.CommandButton btEnemigos 
         Caption         =   "Enemigos"
         Height          =   375
         Left            =   1680
         TabIndex        =   5
         Top             =   1320
         Width           =   975
      End
      Begin VB.CommandButton btObjetos 
         Caption         =   "Objetos"
         Height          =   375
         Left            =   1680
         TabIndex        =   4
         Top             =   120
         Width           =   975
      End
      Begin VB.CommandButton btPersonajes 
         Caption         =   "Personajes"
         Height          =   375
         Left            =   1680
         TabIndex        =   3
         Top             =   720
         Width           =   975
      End
      Begin VB.CommandButton btArmas 
         Caption         =   "Armas"
         Height          =   375
         Left            =   240
         TabIndex        =   2
         Top             =   1320
         Width           =   975
      End
      Begin VB.CommandButton btMagias 
         Caption         =   "Magias"
         Height          =   375
         Left            =   240
         TabIndex        =   1
         Top             =   120
         Width           =   975
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00FFFFFF&
         X1              =   2880
         X2              =   2880
         Y1              =   1680
         Y2              =   120
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00FFFFFF&
         X1              =   1440
         X2              =   1440
         Y1              =   1680
         Y2              =   120
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const status = "Pre-Alpha"

Private Sub btMagias_Click()
    frmMagias.Show 0, Me
End Sub

Private Sub Form_Load()
    
    Me.Caption = "SoD ToolKit --- " & status & " " & App.Major & "." & App.Minor & "." & App.Revision
End Sub
